# Chapel Dragonfly Wedding Registry

## Table Contents: 
1. [System Diagram](#system-diagram) 
2. [Future Features](#future-features)
3. [Set Up Secret and Public API keys](#set-up-secret-and-public-api-keys)
   1. [SnipCart](#snipcart)
   2. [Firebase](#firebase)
4. [Setup Config Files](#setup-config-files)
   1. [Firebase Storage](#firebase-storage)
   2. [Vue Global](#vue-global)
   3. [Vue Development](#vue-development)
   4. [Vue Production](#vue-production)
5. [Setup Snipcart Webhooks & Domain/URLs](#setup-snipcart-webhooks--domainurls)
   1. [Webhooks](#webhooks)
   2. [Domain/URLs](#domainurls)
6. [Stripe Merchant Guide](#snipcart-merchant-guide)
   1. [Payment Gateway Integration](#payment-gateway-integration) 
7. [Wedding Registry Guide](#wedding-registry-guide)
   1. [Wedding Registry Creator](#wedding-registry-creator)
   2. [Couples Page](#couples-page)

## System Diagram:
```mermaid
flowchart TB

A((Couple))
A.2((Buyer))
B(Data)
C(Registry Items)
C.2(RegistryItems.json)
D(Wedding Data)
E[(Firebase Firestore)]
E.2[(Firebase Storage)]
F[(Snipcart)]
G{Validation}
G.2{Order Validation}
H[Throw Error]
H.2[Throw Error]
J(Cart)
P[Order Success]

A -->|Input Information|B
B --> G
G -->|Yes| E
G -->|No| H
E --> C & C.2 & D
subgraph coupleInfo
B 
end

C.2 --> E.2
subgraph couplesPage
  C
  C.2
  D
end

subgraph thirdParties
E
E.2
F
end

A.2 -->|Buy Registry Items| C
C --> J
J --> F 
F --> G.2  
G.2 --> |yes|P
G.2 --> |No|H.2 

subgraph checkout
J
end
```

## Future Features
- adding, deleting, or editing items through a UI
- the ability to manually pause items in that UI (block people from buying but still display)
- the ability for items to pause automatically at certain dates before the event (eg T-2 weeks before the wedding, a barber option would be paused). This “pause on” option would be optional
- a list of our inventory for more efficient inputting and standardization
- you’d mentioned maturing the UI with logins and being able to edit the events and all through the registry itself
- messaging couples, though I think you mentioned this would cost per email or text. If that’s the case, we don’t see additional value in doing that

## Set Up Secret and Public API keys
### SnipCart:
- Follow the following instructions to access and setup your SnipCart API/Secret keys: 
  1. Signup with a snipcart account using the following [link](https://app.snipcart.com/register)
  2. Once you've signed up with SnipCart, login to your account useing the following [link](https://app.snipcart.com/)
  3. Now that you're logged in, follow the steps below to access your API Keys and Setup your Secret API Key:
     1. Select "API KEYS" under account list:
      ![Scroll to API](/source/images/snipcart_setup/scroll_to.png)
     2. To Accces your "PUBLIC KEY": 
      ![Snipcart Public Key](/source/images/snipcart_setup/public_key.png)
     3. Create your "SECRET KEY"
      ![Snipcart Secret Key](/source/images/snipcart_setup/secret_key.png)

- For additional SnipCart API resource click the [here](https://docs.snipcart.com/v3/api-reference/introduction)
   
### Firebase:
- Follow the following instructions to access and setup your firebase project API keys: 
  1. Login to firebase [console](https://firebase.google.com/) using your google account (if you dont have a google account you can signup [here](https://accounts.google.com/signup/v2/webcreateaccount?service=mail&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ltmpl=default&gmb=exp&biz=false&flowName=GlifWebSignIn&flowEntry=SignUp))
  ![Firebase Console](/source/images/firebase_setup/sign.png) 
  2. Once you've signed into the firebase console you will need to create project to generate your firebase API keys, follow the steps below to generate your API keys:
     1. Create your project:
       ![Firebase Create Project](/source/images/firebase_setup/create.png)
     2. Input project name: 
       ![Firebase Project Name](/source/images/firebase_setup/name.png)
     3. Disable Google Analytics
       ![Firebase Disable](/source/images/firebase_setup/disable.png)
     4. Access Project Settings
       ![Firebase Access Project Settings](/source/images/firebase_setup/settings.png)
     5. Create Web App & Obtain Firebase API keys
       ![Firebase Create Web App](/source/images/firebase_setup/obtain.png)

## Setup Config Files
Please reference [Set Up Secret and Public API keys](#set-up-secret-and-public-api-keys) to obtain your API keys. 

### Firebase Storage:
- Open `~/functions/configs.json` and change the following configs:
  ```json
  {
    "storage": "REPLACE_WITH_YOUR_STORAGE_BUCKET"
  }
  ```

### Vue Global:
- Open `~/chapel-dragonfly-weding/.env` and change the following configs:
  
  ```markdown
  #########################
  # GLOBAL ENV VARIBLES 
  #########################

    # FIREBASE CONFIG 
    VUE_APP_API_KEY=<REPLACE_WITH_YOUR_SNIPCART_API_KEY>
    VUE_APP_AUTH_DOMAIN=<REPLACE_WITH_YOUR_SNIPCART_AUTH_DOMAIN>
    VUE_APP_PROJECT_ID=<REPLACE_WITH_YOUR_SNIPCART_PROJECT_ID>
    VUE_APP_STORAGE_BUCKET=<REPLACE_WITH_YOUR_SNIPCART_STORAGE_BUCKET>
    VUE_APP_MESSAGING_SENDER_ID=<REPLACE_WITH_YOUR_SNIPCART_MESSAGING_SENDER_ID>
    VUE_APP_APP_ID=<REPLACE_WITH_YOUR_SNIPCART_APP_ID>

    # SNIPCART CONFIG
    VUE_APP_SNIPCART_SECRET_KEY=<REPLACE_WITH_YOUR_SNIPCART_SECRET_KEY>
  ```

### Vue Development:
- Open `~/chapel-dragonfly-weding/.env.development` and change the following configs:
  ```markdown
  #########################
  # DEVELOPMENT ENV VARIABLES 
  #########################

    # SNIPCART CONFIG
    VUE_APP_SNIPCART_SECRET_KEY=<REPLACE_WITH_YOUR_SNIPCART_SECRET_KEY>
  ```

### Vue Production:
- Open `~/chapel-dragonfly-weding/.env.production` and change the following configs 
  ```markdown
  #########################
  # PRODUCTION ENV VARIABLES 
  #########################

    # SNIPCART CONFIG
    VUE_APP_SNIPCART_SECRET_KEY=<REPLACE_WITH_YOUR_SNIPCART_PUBLIC_KEY>
  ```


###









## Setup Snipcart Webhooks & Domain/URLs
### Webhooks
- Following the the steps below to inetrgate your custom snipcart webhook:
  1. Open your account settings ![Open Account](/source/images/snipcart_setup/open_account.png)
  2. Click webhooks in the selection menu ![Select Webhook](/source/images/snipcart_setup/click_webhook.png)
  3. Once you're at the webhook section, paste in the custom snipcart webhook and hit save ![Paste Webhook URL](/source/images/snipcart_setup/paste_webhook.png)

### Domain/URLs
  1. Open your account settings ![Open Account](/source/images/snipcart_setup/open_account.png)
  2. Click domain and URLs in the selection menu ![Select Domain](/source/images/snipcart_setup/click_domain.png)
  3. Set the protocol to `https` ![Select https](/source/images/snipcart_setup/click_https.png)
  4. Copy this firebase storage api url `firebasestorage.googleapis.com` and paste in the url into the selction box then hit save ![Paste Firebase Storage API ](/source/images/snipcart_setup/paste_domain.png)

## Snipcart Merchant Guide
### Payment Gateway Integration 
- To integrate any e-commernce payment gateways please refer to the following [section](https://docs.snipcart.com/v3/dashboard/paypal-configuration) in SnipCarts Documentation.
  
- The following payment gateways are currently avaiable to integrate with SnipCart:
  - [Stripe](https://stripe.com/)
  - [PayPal](https://www.paypal.com/us/home)
  - [Authorize.Net](https://account.authorize.net/ui/themes/anet/oauth/PricingPage.aspx?resellerId=98464)
  - [Braintree](https://www.braintreepayments.com/)
  - [Mollie](https://www.mollie.com/en)
  - [Square](https://squareup.com/us/en)
  - [Custom payment gateways](https://docs.snipcart.com/v3/custom-payment-gateway)
- For Other Merchant Dashboard feature please refer to [SnipCarts Documentation](https://docs.snipcart.com/v3/dashboard/navigation)


## Wedding Registry Guide
### Wedding Registry Creator
  - Partner 1 Form
    - ![Partner 1 Form](/source/images/wedding_registry_interface/partner1.png)
  - Partner 2 Form
    - ![Partner 2 Form](/source/images/wedding_registry_interface/partner2.png) 
  - Couples Page Input
    - ![Couples Page](/source/images/wedding_registry_interface/couples.png) 
  - Adding Items
    - ![Items Form](/source/images/wedding_registry_interface/items.png) 
  - Create Couples page 
    - ![Create Couples Page](/source/images/wedding_registry_interface/create.png) 

### Couples Page
  - Couples Info Section
    - ![Couples Info Section](/source/images/couples_page/info.png) 
  - Item Info Section 
    - ![Item Info Section](/source/images/couples_page/item_info.png) 
  - Customer Feed 
    - ![Customer Feed](/source/images/couples_page/feed.png) 


