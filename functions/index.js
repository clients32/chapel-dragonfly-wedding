const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp();

const fs = require('fs');
const tmp = require('os').tmpdir();
const { promisify } = require('util');
const writeFileAsync = promisify(fs.writeFile);

const path = require('path');
const cors = require('cors')({ origin: true });

//firebase configs
const firebaseConfig = require('./config.json');
const firebaseStorage = admin.storage();

const db = admin.firestore();

//firestore collections
const couplesRef = db.collection('couples');
const customersRef = db.collection('customers');
const ordersRef = db.collection('orders');

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions

// Snipcart Webhook
exports.snipcartWebook = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    try {
      //store request event name
      const event = req.body.eventName;

      //enum event types
      const ORDER_COMPLETE = 'order.completed';

      switch (event) {
        case ORDER_COMPLETE:
          const items = req.body.content.items;

          let error = {};
          let temp;
          let validatedCouplesID;

          //Validate couples ID
          items.forEach((item) => {
            //if temp is not set the first couplesID
            if (!temp) {
              temp = item.metadata.couplesID;
            }

            //checks if couplesID is different from the other items
            if (temp !== item.metadata.couplesID) {
              error.message = 'CODE does not equal abort!';
              error.status = 404;

              throw error;
            }

            //store validated coulesID
            validatedCouplesID = temp;
          });

          //collect customerData to store into couples collection
          const customerData = {
            customerID: req.body.content.userId,
            name: req.body.content.user.billingAddress.name,
            email: req.body.content.user.email,
            order: {
              orderID: req.body.content.token,
              items: () => {
                return items.map((item) => {
                  return {
                    id: item.id,
                    name: item.name,
                    giftNote: item.customFields[0].displayValue,
                    quantity: item.quantity
                  };
                });
              }
            }
          };

          //create orders doc and set order
          const orderDoc = ordersRef.doc(customerData.order.orderID);
          await orderDoc.set({
            id: customerData.order.orderID,
            items: customerData.order.items()
          });

          //get couples doc and add customer to couplesDoc
          const coupleDoc = couplesRef.doc(validatedCouplesID);
          const coupleSnapshot = await coupleDoc.get();

          //if no customers create customer
          if (coupleSnapshot.data().customers.length <= 0) {
            //create customer
            const customerDoc = customersRef.doc(customerData.customerID);
            await customerDoc.set({
              customerID: customerData.customerID,
              name: customerData.name,
              email: customerData.email,
              orders: [orderDoc]
            });

            //add new customer
            await coupleDoc.update({
              customers: admin.firestore.FieldValue.arrayUnion(customerDoc)
            });
          } else {
            let noMatch = 0;

            //wait and loop through customers to check if customerRef already exists in customers
            const foundCustomer = new Promise((resolve, reject) => {
              coupleSnapshot.data().customers.forEach(async (customerRef) => {
                try {
                  const customerSnapshot = await customerRef.get();

                  if (
                    customerSnapshot.data().customerID ===
                    customerData.customerID
                  ) {
                    return resolve(customerRef);
                  }

                  noMatch += 1;

                  if (noMatch === coupleSnapshot.data().customers.length) {
                    return resolve(undefined);
                  }
                } catch (err) {
                  reject(err);
                }
              });
            });

            //wait till customer is found
            const customerRef = await foundCustomer;

            //if customer exists in couple add new order to existing customer orders
            if (customerRef) {
              await customerRef.update({
                orders: admin.firestore.FieldValue.arrayUnion(orderDoc)
              });
            } else {
              //create new customer
              const customerDoc = customersRef.doc(customerData.customerID);
              await customerDoc.set({
                customerID: customerData.customerID,
                name: customerData.name,
                email: customerData.email,
                orders: [orderDoc]
              });

              //add new customer to couple
              await coupleDoc.update({
                customers: admin.firestore.FieldValue.arrayUnion(customerDoc)
              });
            }
          }

          break;

        default:
          console.log(`$Unhandled event type: "${event}"`);
      }

      return res.status(200).send({
        message: 'Completed Webhook Request!'
      });
    } catch (err) {
      return err;
    }
  });
});

// Upload Products
exports.uploadProductsJSON = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    try {
      //incoming object request
      let { couplesID, products, env } = req.body;

      //take data and convert to JSON format
      const formatedData = JSON.stringify(products);

      //create dir path
      const filePath = path.join(tmp + '/products.json');
      const bucketFilePath = `couples/${couplesID}/products/products.json`;

      //write products.json file
      await writeFileAsync(filePath, formatedData);

      //write file to dev cloud storage
      await firebaseStorage
        .bucket(
          env === 'development'
            ? firebaseConfig.storage.dev
            : firebaseConfig.storage.prod
        )
        .upload(filePath, {
          destination: bucketFilePath
        });

      //return status '200' code
      return res.sendStatus(200);
    } catch (err) {
      console.log(err);
      return err;
    }
  });
});

//Update Products
exports.updateProductsJSON = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    try {
      //incoming object request
      let { couplesID, products, url, env } = req.body;

      let updateProducts = [];

      products.forEach((product) => {
        product = {
          id: product.id,
          price: product.price,
          url
        };

        updateProducts.push(product);
      });

      //take data and convert to JSON format
      const formatedData = JSON.stringify(updateProducts);

      //create dir path
      const filePath = path.join(tmp + '/products.json');
      const bucketFilePath = `/couples/${couplesID}/products/products.json`;

      //write products.json file
      await writeFileAsync(filePath, formatedData);

      //write file to dev cloud storage
      await firebaseStorage
        .bucket(
          env === 'development'
            ? firebaseConfig.storage.dev
            : firebaseConfig.storage.prod
        )
        .upload(filePath, {
          destination: bucketFilePath
        });

      //return '200' status code
      return res.sendStatus(200);
    } catch (err) {
      console.log(err);
      return err;
    }
  });
});
